<?php
require_once('animal.php');
require_once('frog.php');
require_once('ape.php');


$sheep = new animal("shaun");
echo "Name : ".$sheep->name."<br>"; 
echo "Legs : ".$sheep->legs."<br>"; // 4
echo "cold blooded : ".$sheep->cold_blooded."<br><br>"; // "no"

$frog1 = new frog("buduk");
echo "Name : ".$frog1->name."<br>"; // "shaun"
echo "Legs : ".$frog1->legs."<br>"; // 4
echo "cold blooded : ".$frog1->cold_blooded."<br>";
echo "Hop Hop ".$frog1->jump()."<br><br>";

$ape1 = new ape("kera sakti");
echo "Name : ".$ape1->name."<br>"; 
echo "Legs : ".$ape1->legs."<br>"; // 4
echo "cold blooded : ".$ape1->cold_blooded."<br>";
echo "Auoo ".$ape1->yell()."<br><br>";

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
?>